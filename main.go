package main

import (
	"flag"

	"github.com/asticode/go-astilog"
	"github.com/pkg/errors"
	"github.com/syntacticsugarglider/go-astilectron"
	"github.com/syntacticsugarglider/go-astilectron-bootstrap"
)

// Vars
var (
	AppName string
	BuiltAt string
	debug   = astilectron.PtrBool(false)
	w       *astilectron.Window
)

func main() {
	// Init
	flag.Parse()
	astilog.FlagInit()

	// Run bootstrap
	if err := bootstrap.Run(bootstrap.Options{
		Asset:    Asset,
		AssetDir: AssetDir,
		AstilectronOptions: astilectron.Options{
			AppName:            AppName,
			AppIconDarwinPath:  "resources/icon.icns",
			AppIconDefaultPath: "resources/icon.png",
		},
		Debug: *debug,
		OnWait: func(a *astilectron.Astilectron, _ []*astilectron.Window, _ *astilectron.Menu, _ *astilectron.Tray, _ *astilectron.Menu) error {
			go func() {
				a.Dock().Hide()
			}()
			return nil
		},
		RestoreAssets: RestoreAssets,
		Windows: []*bootstrap.Window{{
			Homepage:       "index.html",
			MessageHandler: handle,
			Options: &astilectron.WindowOptions{
				Center:         astilectron.PtrBool(false),
				Height:         astilectron.PtrInt(220),
				Width:          astilectron.PtrInt(200),
				Frame:          astilectron.PtrBool(false),
				Movable:        astilectron.PtrBool(true),
				Resizable:      astilectron.PtrBool(false),
				Fullscreen:     astilectron.PtrBool(false),
				Fullscreenable: astilectron.PtrBool(false),
				Transparent:    astilectron.PtrBool(true),
				Minimizable:    astilectron.PtrBool(false),
				Title:          astilectron.PtrStr("Monolith"),
			},
		}},
	}); err != nil {
		astilog.Fatal(errors.Wrap(err, "running bootstrap failed"))
	}
}

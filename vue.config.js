const path = require("path");

module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        "@": path.resolve(__dirname, "./ui")
      },
      extensions: ["*", ".vue", ".ts", ".otf"]
    }
  },
  pages: {
    index: {
      entry: "ui/main.ts",
      template: "ui/assets/index.html"
    }
  },
  outputDir: path.resolve(__dirname, "./resources/app"),
  assetsDir: "static",
  baseUrl: ""
};

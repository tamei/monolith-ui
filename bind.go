package main

//Asset is a shim
func Asset(name string) ([]byte, error) {
	return []byte{}, nil
}

//AssetDir is a shim
func AssetDir(name string) ([]string, error) {
	return []string{}, nil
}

//RestoreAssets is a shim
func RestoreAssets(dir, name string) error {
	return nil
}

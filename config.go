package main

import (
	"os"
	"path"

	yaml "gopkg.in/yaml.v2"
)

func writeState() error {
	data, err := yaml.Marshal(state)
	if err != nil {
		return err
	}
	f, err := os.Create(path.Join(mPath, "monolith.yml"))
	defer f.Close()
	if err != nil {
		return err
	}
	_, err = f.Write(data)
	if err != nil {
		return err
	}
	return nil
}

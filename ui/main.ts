import Vue from "vue";
import App from "./App.vue";
import router from "./router";

Vue.config.productionTip = false;

import Ripple from "vue-ripple-directive";

Vue.directive("ripple", Ripple);

Vue.component("add-icon");

new Vue({
  router,
  render: (h) => h(App)
}).$mount("#app");

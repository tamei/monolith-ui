GOCMD=go
GOBUILD=astilectron-bundler -v
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
all: ui build
.PHONY: ui
ui:
	yarn run build
build: 
	mv bind.go _bind.go
	$(GOBUILD) -c bundler.json && rm -f ./bind* || mv _bind.go bind.go
	mv _bind.go bind.go
test: 
	$(GOTEST) -v ./...
clean: 
	$(GOCLEAN)
	rm -rf ./output
	rm -rf ./resources/app
linux:
	mv bind.go _bind.go
	$(GOBUILD) -c bundler-linux.json && rm -f ./bind* || mv _bind.go bind.go
	mv _bind.go bind.go
darwin:
	mv bind.go _bind.go
	$(GOBUILD) -c bundler-darwin.json && rm -f ./bind* || mv _bind.go bind.go
	mv _bind.go bind.go
windows:
	mv bind.go _bind.go
	$(GOBUILD) -c bundler-windows.json && rm -f ./bind* || mv _bind.go bind.go
	mv _bind.go bind.go
deps:
	go get -v
	yarn install
app-deps:
	go get -v
ui-deps:
	yarn install
serve:
	yarn serve

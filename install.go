package main

import (
	"os"
	"path"
	"path/filepath"

	"github.com/otiai10/copy"
)

func installHandle(e error) bool {
	if e != nil {
		window.SendMessage(response{
			"status": "failure",
			"data":   "installation failure",
		})
		return true
	}
	return false
}

func install() {
	ex, err := os.Executable()
	if installHandle(err) {
		return
	}
	exPath := path.Dir(path.Dir(filepath.Dir(ex)))
	err = copy.Copy(exPath, path.Join(path.Join(path.Dir(mPath), "Applications"), "Monolith.app"))
	if installHandle(err) {
		return
	}
	state.Installed = true
	os.RemoveAll(exPath)
	err = writeState()
	if !installHandle(err) {
		window.SendMessage(response{
			"status": "success",
			"data":   "installed",
		})
	}
}

# Package manager and project tooling for Monolith

## Project setup

```
make deps
```

### Serve UI on dev server for testing

```
make serve
```

### Build for production

```
make build
```

### Cross-compile to Linux

```
make build-linux
```

### Cross-compile to macOS

```
make build-darwin
```

### Cross-compile to Windows

```
make build-windows
```

### Clean artifacts

```
make clean
```

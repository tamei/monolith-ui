package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"os/user"
	"path"
	"path/filepath"
	"strings"

	"github.com/syntacticsugarglider/go-astilectron"
	"github.com/syntacticsugarglider/go-astilectron-bootstrap"
	yaml "gopkg.in/yaml.v2"
)

type response map[string]interface{}

//State represents the stored application state
type State struct {
	Engines   []string
	Installed bool
}

var state State

var window *astilectron.Window

var mPath string

func handle(w *astilectron.Window, m bootstrap.MessageIn) (payload interface{}, err error) {
	if window == nil {
		window = w
	}
	switch m.Name {
	case "initialize":
		payload = initialize()
	case "close":
		w.Close()
		os.Exit(0)
	case "restart":
		exec.Command("bash", "-c", fmt.Sprintf("open -F %s", path.Join(path.Join(path.Dir(mPath), "Applications"), "Monolith.app"))).Output()
		w.Close()
		os.Exit(0)
	}
	return
}

func initialize() interface{} {
	out, err := exec.Command("bash", "-c", "git --version").Output()
	if err != nil {
		return response{
			"status": "failure",
			"data":   fmt.Sprintf("%v", err),
		}
	}
	k := strings.Split(string(out), " ")
	if len(k) < 2 || k[1] != "version" {
		return response{
			"status": "failure",
			"data":   "no git",
		}
	}
	if !connected() {
		return response{
			"status": "failure",
			"data":   "offline",
		}
	}
	usr, err := user.Current()
	if err != nil {
		return response{
			"status": "failure",
			"data":   "state inaccessible",
		}
	}
	dir := usr.HomeDir
	e, _ := exists(path.Join(dir, ".monolith"))
	if !e {
		err := os.Mkdir(path.Join(dir, ".monolith"), os.ModePerm)
		if err != nil {
			return response{
				"status": "failure",
				"data":   "state inaccessible",
			}
		}
	}
	mPath = path.Join(dir, ".monolith")
	e, _ = exists(path.Join(dir, ".monolith"))
	if !e {
		return response{
			"status": "failure",
			"data":   "state inaccessible",
		}
	}
	if e, _ = exists(path.Join(dir, ".monolith", "monolith.yml")); e {
		source, err := ioutil.ReadFile(path.Join(dir, ".monolith", "monolith.yml"))
		if err != nil {
			return response{
				"status": "failure",
				"data":   "state inaccessible",
			}
		}
		err = yaml.UnmarshalStrict(source, &state)
		if err != nil {
			return response{
				"status": "failure",
				"data":   "state corrupted",
			}
		}
		f, err := os.Create(path.Join(dir, ".monolith", "monolith.yml"))
		defer f.Close()
		if err != nil {
			return response{
				"status": "failure",
				"data":   "state inaccessible",
			}
		}
		f.Write(source)
	} else {
		state = State{}
		data, err := yaml.Marshal(state)
		if err != nil {
			return response{
				"status": "failure",
				"data":   "state corrupted",
			}
		}
		f, err := os.Create(path.Join(dir, ".monolith", "monolith.yml"))
		defer f.Close()
		f.Write(data)
		f.Close()
	}
	if !state.Installed {
		go install()
		return response{
			"status": "success",
			"data":   "installing",
		}
	}
	ex, err := os.Executable()
	if err != nil {
		return response{
			"status": "failure",
			"data":   "bad executable",
		}
	}
	if path.Dir(path.Dir(path.Dir(filepath.Dir(ex)))) != path.Join(dir, "Applications") {
		o, err := exec.Command("bash", "-c", fmt.Sprintf("diff -qr ~/Applications/Monolith.app %s", path.Dir(path.Dir(filepath.Dir(ex))))).Output()
		rl := ""
		if len(strings.Split(string(o), " ")) > 1 {
			rl = strings.Split(string(o), " ")[len(strings.Split(string(o), " "))-1]
		}
		if o != nil && len(rl) > 1 && rl[:len(rl)-1] == "differ" {
			state.Installed = false
			writeState()
			os.RemoveAll(path.Join(dir, "Applications", "Monolith.app"))
			_, _ = exec.Command("bash", "-c", fmt.Sprintf("open -F %s", path.Dir(path.Dir(filepath.Dir(ex))))).Output()
			window.SendMessage(response{
				"status": "close",
			})
			return response{}
		}
		o, err = exec.Command("bash", "-c", fmt.Sprintf("open -F %s", path.Join(dir, "Applications", "Monolith.app"))).Output()
		if err != nil {
			return response{
				"status": "failure",
				"data":   "state corrupted",
			}
		}
		os.RemoveAll(path.Dir(path.Dir(filepath.Dir(ex))))
		return response{
			"status": "close",
		}
	}
	return response{
		"status": "success",
		"data":   "updating",
	}
}

func connected() bool {
	_, err := http.Get("https://github.com")
	if err != nil {
		return false
	}
	return true
}

func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}
